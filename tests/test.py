import subprocess


def call(*args):
    return subprocess.run(['d', *args], capture_output=True, text=True)


def test_basic():
    res = call('meow', 'arg')
    assert res.returncode == 0
    assert res.stdout == 'meow arg\n'


def test_nocommand():
    res = call()
    assert res.returncode == 0
    assert res.stdout == '\n'


def test_builtin_alias():
    res = call('r', '80')
    assert res.returncode == 0
    assert res.stdout == 'runserver 80\n'


def test_home_alias():
    res = call('homealias', 'arg')
    assert res.returncode == 0
    assert res.stdout == 'homecommand arg\n'


def test_project_alias():
    res = call('projectalias', 'arg')
    assert res.returncode == 0
    assert res.stdout == 'projectcommand arg\n'


def test_bad_alias():
    res = call('badalias', 'arg')
    assert res.returncode == 1
    assert res.stderr == "django-shts3: can't parse alias: ''' (No closing quotation)\n"
